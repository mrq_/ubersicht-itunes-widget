![Screen Shot 2014-10-09 at 11.59.39.png](https://bitbucket.org/repo/bnaaEx/images/2115211594-Screen%20Shot%202014-10-09%20at%2011.59.39.png)

A widget that shows what's currently playing in iTunes.

Assembled by Piotr Gajos
[https://github.com/Pe8er/Ubersicht-Widgets](https://github.com/Pe8er/Ubersicht-Widgets)

Code pieces pulled from Chris Johnson's World Clock widget, and Album Art from here : [http://stackoverflow.com/a/16998373](http://stackoverflow.com/a/16998373)

Modified by Me.