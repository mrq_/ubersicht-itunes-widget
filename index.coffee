
# A widget that shows what's currently playing in iTunes.
# Widget Assembled by Piotr Gajos
# https://github.com/Pe8er/Ubersicht-Widgets
# 
# I don't know how to write code, so I obviously pulled 
# pieces from all over the place, particularly from Chris Johnson's World Clock widget. 
# Also big thanks to Josh "Baby Boi" Rutherford.
#
#
# Removed Spotifiy, and Song Progress as it wasn't working on my side.
# Added Artwork.

command: "osascript 'iTunes.widget/script/currentSong.scpt'"

refreshFrequency: 1000

style: """
color: #FFFFFF
font-family: Godzilla,Helvetica,Arial,sans-serifx
font-weight: 500
left: 2.5%
line-height: 1.1
margin: 0
text-shadow: 2px 2px #ff0000
top: 5%
width: 530px

.truncate 
  width: 90%
  white-space: nowrap
  overflow: hidden
  padding: 0em 0.5em
  text-overflow: ellipsis

.block 
  background-color: rgba(255,255,255,0.2)
  padding: 0.01em 0em 1.5em

.title
  font-size: 1em
  text-align: center
  width: 100%

.inner-block
  width: 100%

.trackInfo
  float: left
  padding-left: 1em
  padding-right: 0.5em
  width: 50%

.trackInfo > p
  margin: 0 0 10px 
  font-size: 1.3em
  line-height: 1.3
  text-align: center

.trackInfo p.artist

.trackInfo p.album
  font-size: 1.05em

.artwork 
  margin-left: auto;
  opacity: 0.65
  padding-right: 20px;
  width: 40%

.artwork img
  height: 190px
  width: 100%

"""

render: -> """"""

# Update the rendered output.
update: (output, domEl) -> 
  # Get our main DIV.
  div = $(domEl)
  # Get our pieces
  values = output.split("~")

  title = values[0]
  artist = values[1]
  album = values[2]
  rating = values[3]
  year = values[4]
  tDuration = values[5]
  cover = values[6]


  # Initialize our HTML.
  medianowHTML = ''
  # Make it disappear if nothing is playing
  if values[0] != 'Nothing playing'
    # Create the DIVs for each piece of data.
    $(domEl).animate({ opacity: 1 }, 500)
    medianowHTML = "
    
    <div class='block'>
        <div class='title'>
            <h1 class='truncate'>" + title + "</h1>
        </div>

        <div class='inner-block'>
          <div class='trackInfo'>
            <p class='artist'>" + artist + "</p>
            <p class='album'>" + album + "</p>
            <p class='rating'>" + rating + "</p>
            <p class='year'>" + year + "</p> 
            <p class='duration'>" + tDuration + "</p>
          </div>
          <div class='artwork'>
            <img src='iTunes.widget/script/img/" + cover + "'/>
          </div>
        </div>
    </div>"
  else
    $(domEl).animate({ opacity: 0 }, 500)

  # Set the HTML of our main DIV.
  div.html(medianowHTML)
eft: 0
      padding-bottom: 0
      padding-left: 0
      padding-right: 0
      padding-top: 0
      position: absolute
      text-align: center
      width: 100%

.truncate 
      width: 520px
      white-space: nowrap
      overflow: hidden
      text-overflow: ellipsis

.progress 
      background-color: #f5f5f5
      border-bottom-left-radius: 4px
      border-bottom-right-radius: 4px
      border-top-left-radius: 4px
      border-top-right-radius: 4px
      box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1) inset
      height: 5px
      margin-bottom: 20px
      overflow-x: hidden
      overflow-y: hidden

.progress-bar 
      background-color: #529bda
      box-shadow: 0 -1px 0 rgba(0, 0, 0, 0.15) inset
      color: #ffffff
      float: left
      font-size: 12px
      height: 100%
      text-align: center
      transition-delay: 0s
      transition-duration: 0.6s
      transition-property: @width
      transition-timing-function: ease
      width: @width

bg-blur = 10px

.bg-slice
      position: absolute
      top: -(bg-blur)
      left: -(bg-blur)
      width: 100% + 2*bg-blur
      height: 100% + 2*bg-blur
      -webkit-filter: blur(bg-blur)


"""

render: (output, domEl)-> """ """

# Update the rendered output.
update: (output, domEl) ->

  # Get our main DIV.
  div = $(domEl)

  # Get our pieces
  values = output.split(" ~ ")

  # Initialize our HTML.
  medianowHTML = ''

  # Progress bar things
  tDuration = values[5]
  tPosition = values[6]
  # console.log ("[Duration -->" + tDuration + "]  [Position -->" + tPosition + "]  ")


  # Make it disappear if nothing is playing
  if values[0] != 'Nothing playing'

    # Create the DIVs for each piece of data.
    $(domEl).animate({ opacity: 1 }, 500)
    medianowHTML = "
    <canvas class='bg-slice'></canvas>
    <div class='container block'>
      <div class='row'>
        <div class='col-md-12 track-detail' style='padding: 0'>
            <h1 class='truncate' style='font-size: 30px; padding-right: 0; padding-left: 5px; text-align: center;'>" + values[1] + "</h1>
        </div>
      </div>

        <div class='row'>
          <div class='col-md-5' style='left: 25px;'>
              <div class='row'>
                  <div class='col-md-12 artist-detail'>
                      <h3>" + values[0] + "</h3>
                      <p>" + values[2] + "</p>
                      <p>" + values[4] + "</p>
                      <p>" + values[3] + "</p>
                      
                  </div>
              </div>
              <div class='row'>
                  
              </div>
          </div>

          <div class='col-md-5'>
                <div class='row'>
                    <div class='col-md-12 artwork' style='background-image: url(iTunes.widget/script/img/" + values[7] + "); left: 45px' >
                      <p class='durate'>" + tPosition + " / " + tDuration + "</p>
                    </div>
                  </div>
                </div>
            </div>
            <div class='row'>
              <div class='col-md-12 track-detail' style='padding: 0'>
                  <h1 style='text-align: center;'></h1>
              </div>
            </div>
        </div>
    </div>"
  else

    $(domEl).animate({ opacity: 0 }, 500)

  # Set the HTML of our main DIV.
  div.html(medianowHTML)

  afterRender: (domEl) ->
    uebersicht.makeBgSlice(el) for el in $(domEl).find '.bg-slice'
